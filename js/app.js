var myApp=angular.module("myApp", []);

myApp.controller("myController", function($scope){

	$scope.newUser= {};
	$scope.clickedUser={};
	$scope.message="";

	$scope.users=[
		{username: "rimon", fullName:"Rimon Biswas", email:"rimon@yahoo.com"},
		{username: "mark", fullName:"Markel Biswas", email:"mark@yahoo.com"},
		{username: "polo", fullName:"Polo Biswas", email:"polo@gmail.com"}

	];

	$scope.saveUser= function(){
		$scope.users.push($scope.newUser);
		$scope.newUser= {};
		$scope.message="New User Added Successfully";

	};

	$scope.selectUser=function(user){
		$scope.clickedUser=user;
	};

	$scope.updateUser=function(){
		$scope.message="User Updated Successfully";

		
	};

	$scope.deleteUser=function(){

		$scope.users.splice($scope.users.indexOf($scope.clickedUser),1);
		$scope.message="User Deleted";

	};

	$scope.clearMessage= function(){
		$scope.message="";

	};






});